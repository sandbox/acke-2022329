(function ($) {
	Drupal.behaviors.smooth_anchor_scroll = {
		attach: function(context) {  
      $('a[href*=#]').click(function(){
        $("html, body").animate({scrollTop: jQuery($(this).attr('href')).offset().top -=110}, 2000, 'swing');
      });
    }
  }
})(jQuery);